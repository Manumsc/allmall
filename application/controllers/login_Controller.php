<?php

Class Login_Controller extends CI_controller{
	
	public function Index()
	{
//$this->load->view('index');
	if( $this->session->userdata('isLoggedIn') ) {
        $this->load->view('logo');
    } else {
       $this->load->view('logo');
    }	
	

	
		
	}
	
	public function validation()
	{
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username','User Name','trim|required|min_length[3]|max_length[20]|xss_clean');
		$this->form_validation->set_rules('password','Password','trim|required|min_length[6]|max_length[20]|matches[repassword]|xss_clean');
		$this->form_validation->set_rules('email','Email','trim|required|min_length[20]|max_length[50]|valid_email|xss_clean');
		$this->form_validation->set_rules('phone','Phone','trim|required|min_length[10]|max_length[10]|xss_clean');
		$this->form_validation->set_rules('repassword','Re Password','trim|required|min_length[6]|max_length[20]|xss_clean');
		
		
		
	
		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('logo');
			
		}
			
		else
		{
			$this->load->model('newModel');
			$f=$this->newModel->insert_value();
			
			if($f == 1){
				
			$this->load->view('new_home');
			}
			else {
				echo "error";
			}
		}
	}
	public function valid_email($email,$code){
		
		$email_code=trim($code);
		$validated=$this->newModel->validate_email($email,$email_code);
		if($validated === 1)
			{
				
				$this->load->view('');
				
			}
			else {
				
					echo 'Error while activating the account please contact admin :'.$this->config->item('admin_mail');
					
					
				}
		
		
		
	}

	public function login_validation()
	{
		$this->load->model('login_model');
		$fl=$this->login_model->login_validate();
		if($fl == 1)
	{
	
	$this->load->view('new_home');
	
	}
	else 
		{
			$data ['error'] = "user name or password doesn't exists";
			
			$this->load->view('logo',$data);
			
		}
	
		
	}
	};
	
?>